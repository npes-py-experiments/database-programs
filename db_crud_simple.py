"""

Code: http://www.py4e.com/code3/db1.py

The code to create a database file and a table named Tracks with two columns in
the database is as follows:
"""

import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/database-programs/databases/')

print(files_path)

# Create and connect to database
conn = sqlite3.connect(files_path / 'music.db')

cur = conn.cursor()

# Create table in database
with conn:
    cur.execute('DROP TABLE IF EXISTS Tracks')
    cur.execute('CREATE TABLE Tracks (id INTEGER, title TEXT, plays INTEGER)')

# Insert rows in tracks table
with conn:
    cur.execute('INSERT INTO Tracks (id, title, plays) VALUES (?, ?, ?)', (1, 'Thunderstruck', 20))
    cur.execute('INSERT INTO Tracks (id, title, plays) VALUES (?, ?, ?)', (2, 'My Way', 15))

# info to user
print('All rows in the Tracks table:')

# select values from table
cur.execute('SELECT id, title, plays FROM Tracks')

# print out the values
for row in cur:
    print(row)
# cur.execute('DELETE FROM Tracks WHERE plays < 100')

with conn:
    cur.execute('UPDATE Tracks SET title = ?, plays = ? WHERE id = 1', ('your way', 200))

cur.execute('SELECT id, title, plays FROM Tracks')

# print out the values
for row in cur:
    print(row)

# Close database connection
conn.close()