import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/databases/')
print(files_path)

try:
    conn = sqlite3.connect(files_path / 'chinook.db')
    print('connected to database')
    cur = conn.cursor()

    sql_query = """SELECT name FROM sqlite_master WHERE type='table';""" 
    
    with conn:
        cur.execute(sql_query)
        print(cur.fetchall())

except sqlite3.Error as e:
    print(f'failed to connect to database, error: {e}')

finally: 
    if conn:
        cur.close()

        # Close database connection
        conn.close()
        print('database connection closed')