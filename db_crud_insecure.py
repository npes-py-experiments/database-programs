"""

Code: http://www.py4e.com/code3/db1.py

The code to create a database file and a table named Tracks with two columns in
the database is as follows:
"""

import sqlite3
from pathlib import Path # read https://realpython.com/python-pathlib/#creating-paths
files_path = Path(str(Path.cwd()) + '/databases/')

print(f'files path = {files_path}')

# Create and connect to database
conn = sqlite3.connect(files_path / 'music.db')

cur = conn.cursor()

# Create table in database
with conn:
    cur.execute('DROP TABLE IF EXISTS Tracks')
    cur.execute('CREATE TABLE Tracks (id INTEGER, title TEXT, plays INTEGER)')

# Insert rows in tracks table
with conn:
    cur.execute('INSERT INTO Tracks (id, title, plays) VALUES (?, ?, ?)', (1, 'Thunderstruck', 20))
    cur.execute('INSERT INTO Tracks (id, title, plays) VALUES (?, ?, ?)', (2, 'My Way', 15))

# info to user
print('All rows in the Tracks table:')

# select values from table
cur.execute('SELECT id, title, plays FROM Tracks')

# print out the values
for row in cur:
    print(row)
# cur.execute('DELETE FROM Tracks WHERE plays < 100')

with conn:
    cur.execute('UPDATE Tracks SET title = ?, plays = ? WHERE id = 1', ('your way', 200))

cur.execute('SELECT id, title, plays FROM Tracks')

# print out the values
for row in cur:
    print(row)

# insecure SQL injection - from https://docs.python.org/3/library/sqlite3.html#how-to-use-placeholders-to-bind-values-in-sql-queries
# '' OR TRUE; --'
injection_attempt = input('Do your best! > ')
print(f'Your injection attempt: {injection_attempt}')
query = f'SELECT * FROM Tracks WHERE id = {injection_attempt}'
print(f'The query looks like this: {query}')
cur.execute(query)

try:
    for row in cur:
        print(row)

except sqlite3.Error as e:
        print(f'Database error: {e}')
        
except KeyboardInterrupt:
    print(f'\nSee ya....')
    exit(0)
    
except Exception as e:
    print(f'Something is very wrong!: {e}')
    exit(1)